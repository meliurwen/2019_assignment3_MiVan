package mivan;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class Mivan {

	public static void main(String[] args) {

		SpringApplication.run(Mivan.class, args);
	}

}
