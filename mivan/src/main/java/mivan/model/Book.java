package mivan.model;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinTable;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;

@Entity
@Table(name = "book")
public class Book implements Serializable {

	private static final long serialVersionUID = 19L;

	@Id
	@Column(name = "isbn", nullable = false, unique = true)
	private long isbn;

	@Column(name = "title", nullable = false)
	private String title;

	@OneToOne
	private Book prequel;

	@ManyToMany
	@JoinTable(name = "book_has_author", joinColumns = @JoinColumn(name = "isbn", referencedColumnName = "isbn"), inverseJoinColumns = @JoinColumn(name = "id", referencedColumnName = "id"))
	private List<Author> authors;

	public Book() {
	}

	public Book(long isbn, String title, Book prequel) {
		this.isbn = isbn;
		this.title = title;
		this.prequel = prequel;
		this.authors = new ArrayList<Author>();
	}

	public long getIsbn() {
		return isbn;
	}

	public String getTitle() {
		return title;
	}

	public Book getPrequel() {
		return prequel;
	}

	public void setIsbn(long isbn) {
		this.isbn = isbn;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public void setPrequel(Book prequel) {
		this.prequel = prequel;
	}

	public List<Author> getAuthors() {
		return this.authors;
	}

	public void addAuthor(Author author) {

		if (!this.authors.contains(author)) {
			this.authors.add(author);
			author.addBook(this);
		}
	}

	public void removeAuthor(Author author) {
		if (this.authors.contains(author)) {
			this.authors.remove(author);
			author.removeBook(this);
		}

	}

}
