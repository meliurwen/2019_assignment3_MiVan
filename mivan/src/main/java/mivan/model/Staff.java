package mivan.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name = "staff")
public class Staff extends Person {

	private static final long serialVersionUID = -5124436115031696628L;

	@Column(name = "idka", nullable = false, unique = true)
	private String idka;

	@Column(name = "ruolo", nullable = false)
	private String ruolo;

	@ManyToOne
	@JoinColumn(name = "location_id", referencedColumnName = "id")
	private Location location;

	public Staff() {
		super();
		// TODO Auto-generated constructor stub
	}

	public Staff(String firstName, String lastName, String city, String idka, String ruolo, Location location) {
		super(firstName, lastName, city);
		this.idka = idka;
		this.ruolo = ruolo;
		setLocation(location);
	}

	public Staff(String firstName, String lastName, String city, String idka, String ruolo) {
		super(firstName, lastName, city);
		this.idka = idka;
		this.ruolo = ruolo;

	}

	public String getIdka() {
		return idka;
	}

	public String getRuolo() {
		return ruolo;
	}

	public Location getLocation() {
		return location;
	}

	public void setIdka(String idka) {
		this.idka = idka;
	}

	public void setRuolo(String ruolo) {
		this.ruolo = ruolo;
	}

	public void setLocation(Location location) {
		this.location = location;
		if (!location.getStaffs().contains(this)) {
			location.getStaffs().add(this);
		}
	}

}
