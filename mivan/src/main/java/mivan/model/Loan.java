package mivan.model;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name = "loan")
public class Loan implements Serializable {

	private static final long serialVersionUID = -5124436115031696628L;

	@Id
	@Column(name = "id", nullable = false, unique = true)
	private long id;

	@Column(name = "date_start", nullable = false)
	private long date_start;

	@Column(name = "date_end", nullable = false)
	private long date_end;

	@Column(name = "state", nullable = false)
	private String state;

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "item")
	private Item item;

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "staff")
	private Staff staff;

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "user")
	private User user;

	public Loan() {
	}

	public Loan(long date_start, long date_end, String state, Item item, Staff staff, User user) {
		this.date_start = date_start;
		this.date_end = date_end;
		this.state = state;
		this.item = item;
		this.staff = staff;
		this.user = user;
	}

	public long getId() {
		return id;
	}

	public long getDate_start() {
		return date_start;
	}

	public long getDate_end() {
		return date_end;
	}

	public String getState() {
		return state;
	}

	public Item getItem() {
		return item;
	}

	public Staff getStaff() {
		return staff;
	}

	public User getUser() {
		return user;
	}

	public void setId(long id) {
		this.id = id;
	}

	public void setDate_start(long date_start) {
		this.date_start = date_start;
	}

	public void setDate_end(long date_end) {
		this.date_end = date_end;
	}

	public void setState(String state) {
		this.state = state;
	}

	public void setItem(Item item) {
		this.item = item;
	}

	public void setStaff(Staff staff) {
		this.staff = staff;
	}

	public void setUser(User user) {
		this.user = user;
	}

}
