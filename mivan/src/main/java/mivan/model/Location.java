package mivan.model;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

@Entity
@Table(name = "location")
public class Location implements Serializable {

	private static final long serialVersionUID = -5124436115031696628L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id", unique = true)
	private Long id;

	@Column(name = "name", nullable = false, unique = true)
	private String name;

	@Column(name = "adress", nullable = false)
	private String adress;

	@OneToMany(mappedBy = "location")
	private List<Staff> staffs;

	protected Location() {
	}

	public Location(String name, String adress) {
		this.name = name;
		this.adress = adress;
		this.staffs = new ArrayList<Staff>();
	}

	public Long getId() {
		return id;
	}

	public String getName() {
		return name;
	}

	public String getAdress() {
		return adress;
	}

	public List<Staff> getStaffs() {
		return staffs;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public void setName(String name) {
		this.name = name;
	}

	public void setAdress(String adress) {
		this.adress = adress;
	}

	public void setStaffs(List<Staff> staffs) {
		this.staffs = staffs;
	}

	public void addStaff(Staff staff) {
		this.staffs.add(staff);
		if (staff.getLocation() != this) {
			staff.setLocation(this);
		}
	}

}
