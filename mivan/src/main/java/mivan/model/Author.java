package mivan.model;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToMany;
import javax.persistence.Table;

@Entity
@Table(name = "author")
public class Author implements Serializable {

	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id", unique = true)
	private long id;

	@Column(name = "name", nullable = false)

	private String name;

	@ManyToMany(mappedBy = "authors")
	private List<Book> books;

	public Author() {
	}

	public Author(String name) {
		super();
		this.name = name;
		this.books = new ArrayList<Book>();
	}

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public List<Book> getBooks() {
		return this.books;
	}

	public void addBook(Book book) {
		if (!this.books.contains(book)) {
			this.books.add(book);
			book.addAuthor(this);
		}
	}

	public void removeBook(Book book) {
		if (this.books.contains(book))
			this.books.remove(book);
		book.removeAuthor(this);
	}

}
