package mivan.repository;

import mivan.model.Item;

public interface ItemRepository extends Repository<Item, Long> {

	public void deleteItemById(Long id);

	public void addItem(Item item);

	public int getSize();

}