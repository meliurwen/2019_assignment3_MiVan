package mivan.repository;

import java.util.List;
import java.util.Optional;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

import mivan.model.Location;
import mivan.model.Staff;

public class StaffRepositoryImpl implements StaffRepository {

	private EntityManagerFactory entityManagerFactory;

	public StaffRepositoryImpl() {
		this.entityManagerFactory = Persistence.createEntityManagerFactory("mivan");
	}

	@Override
	public Optional<Staff> findById(Long id) {
		final EntityManager entityManager = this.entityManagerFactory.createEntityManager();
		Staff staff = entityManager.find(Staff.class, id);
		entityManager.close();
		return Optional.ofNullable(staff);
	}

	@Override
	public Iterable<Staff> findAll() {
		final EntityManager entityManager = this.entityManagerFactory.createEntityManager();
		List<Staff> staffs = entityManager.createQuery("FROM Staff", Staff.class).getResultList();
		entityManager.close();
		return staffs;
	}

	public void deleteStaffById(Long id) {
		final EntityManager entityManager = this.entityManagerFactory.createEntityManager();
		try {
			if (!entityManager.getTransaction().isActive()) {
				entityManager.getTransaction().begin();
			}
			Staff staff = entityManager.find(Staff.class, id);
			entityManager.remove(staff);
			entityManager.getTransaction().commit();
			entityManager.close();
		} catch (Exception ex) {
			entityManager.getTransaction().rollback();
		}
	}

	public void addStaff(Staff staff) {
		final EntityManager entityManager = this.entityManagerFactory.createEntityManager();
		try {
			if (!entityManager.getTransaction().isActive()) {
				entityManager.getTransaction().begin();
			}
			entityManager.persist(staff);
			entityManager.getTransaction().commit();
			entityManager.close();
		} catch (Exception ex) {
			entityManager.getTransaction().rollback();
		}
	}

	public void updateStaff(Long id, String firstname, String lastname, String city, String idka, String ruolo,
			Location location) {
		final EntityManager entityManager = this.entityManagerFactory.createEntityManager();
		try {
			if (!entityManager.getTransaction().isActive()) {
				entityManager.getTransaction().begin();
			}
			Staff staff = entityManager.find(Staff.class, id);
			staff.setFirstName(firstname);
			staff.setLastName(lastname);
			staff.setCity(city);
			staff.setIdka(idka);
			staff.setRuolo(ruolo);
			staff.setLocation(location);

			entityManager.persist(staff);
			entityManager.getTransaction().commit();
			entityManager.close();
		} catch (Exception ex) {
			entityManager.getTransaction().rollback();
		}
	}

	public Staff searchStaffByidka(String idka) {
		final EntityManager entityManager = this.entityManagerFactory.createEntityManager();

		Staff staff = null;
		try {
			staff = (Staff) entityManager.createQuery("FROM Staff u WHERE lower(u.idka) = '" + idka.toLowerCase() + "'")
					.getSingleResult();

			entityManager.close();
		} catch (Exception ex) {
			staff = null;
		}

		return staff;
	}

	public int getSize() {
		final EntityManager entityManager = this.entityManagerFactory.createEntityManager();
		int size = 0;
		try {
			size = entityManager.createQuery("FROM Staff").getResultList().size();

			entityManager.close();
		} catch (Exception ex) {
			size = 0;
		}
		return size;
	}
}
