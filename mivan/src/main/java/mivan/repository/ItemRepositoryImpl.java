package mivan.repository;

import java.util.List;
import java.util.Optional;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

import mivan.model.Item;

public class ItemRepositoryImpl implements ItemRepository {
	private EntityManagerFactory entityManagerFactory;

	public ItemRepositoryImpl() {
		this.entityManagerFactory = Persistence.createEntityManagerFactory("mivan");
	}

	@Override
	public Optional<Item> findById(Long id) {
		final EntityManager entityManager = this.entityManagerFactory.createEntityManager();
		Item item = entityManager.find(Item.class, id);
		entityManager.close();
		return Optional.ofNullable(item);
	}

	@Override
	public Iterable<Item> findAll() {
		final EntityManager entityManager = this.entityManagerFactory.createEntityManager();
		List<Item> items = entityManager.createQuery("FROM Item", Item.class).getResultList();
		entityManager.close();
		return items;
	}

	@Override
	public void deleteItemById(Long id) {
		final EntityManager entityManager = this.entityManagerFactory.createEntityManager();
		try {
			if (!entityManager.getTransaction().isActive()) {
				entityManager.getTransaction().begin();
			}
			Item item = entityManager.find(Item.class, id);
			entityManager.remove(item);
			entityManager.getTransaction().commit();
			entityManager.close();
		} catch (Exception ex) {
			entityManager.getTransaction().rollback();
		}
	}

	@Override
	public void addItem(Item item) {
		final EntityManager entityManager = this.entityManagerFactory.createEntityManager();
		try {
			if (!entityManager.getTransaction().isActive()) {
				entityManager.getTransaction().begin();
			}
			entityManager.persist(item);
			entityManager.getTransaction().commit();
			entityManager.close();

		} catch (Exception ex) {
			entityManager.getTransaction().rollback();
		}

	}

	@Override
	public int getSize() {
		final EntityManager entityManager = this.entityManagerFactory.createEntityManager();
		int size = 0;
		try {
			size = entityManager.createQuery("FROM Item").getResultList().size();

			entityManager.close();
		} catch (Exception ex) {
			size = 0;
		}
		return size;
	}

}