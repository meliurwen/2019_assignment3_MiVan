package mivan.repository;

import mivan.model.Location;
import mivan.model.Staff;

public interface StaffRepository extends Repository<Staff, Long> {

	public void deleteStaffById(Long id);

	public void addStaff(Staff user);

	public void updateStaff(Long id, String firstname, String lastname, String city, String idka, String ruolo,
			Location location);

	public Staff searchStaffByidka(String idka);

	// public List<Staff> searchStaffByRole(String role);

	public int getSize();
}
