package mivan.repository;

import java.util.List;
import java.util.Optional;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

import mivan.model.Location;
import mivan.model.Staff;

public class LocationRepositoryImpl implements LocationRepository {

	private EntityManagerFactory entityManagerFactory;

	public LocationRepositoryImpl() {
		this.entityManagerFactory = Persistence.createEntityManagerFactory("mivan");
	}

	@Override
	public Optional<Location> findById(Long id) {
		final EntityManager entityManager = this.entityManagerFactory.createEntityManager();
		Location location = entityManager.find(Location.class, id);
		entityManager.close();
		return Optional.ofNullable(location);
	}

	@Override
	public Iterable<Location> findAll() {
		final EntityManager entityManager = this.entityManagerFactory.createEntityManager();
		List<Location> location = entityManager.createQuery("FROM Location", Location.class).getResultList();
		entityManager.close();
		return location;
	}

	public void deleteLocationById(Long id) {
		final EntityManager entityManager = this.entityManagerFactory.createEntityManager();
		try {
			if (!entityManager.getTransaction().isActive()) {
				entityManager.getTransaction().begin();
			}
			Location location = entityManager.find(Location.class, id);
			entityManager.remove(location);
			entityManager.getTransaction().commit();
			entityManager.close();
		} catch (Exception ex) {
			entityManager.getTransaction().rollback();
		}
	}

	public void addLocation(Location location) {
		final EntityManager entityManager = this.entityManagerFactory.createEntityManager();
		try {
			if (!entityManager.getTransaction().isActive()) {
				entityManager.getTransaction().begin();
			}
			entityManager.persist(location);
			entityManager.getTransaction().commit();
			entityManager.close();

		} catch (Exception ex) {
			entityManager.getTransaction().rollback();
		}
	}

	public void updateLocation(Long id, String name, String adress, List<Staff> staffs) {
		final EntityManager entityManager = this.entityManagerFactory.createEntityManager();
		try {
			if (!entityManager.getTransaction().isActive()) {
				entityManager.getTransaction().begin();
			}
			Location location = entityManager.find(Location.class, id);
			location.setName(name);
			location.setAdress(adress);
			location.setStaffs(staffs);
			entityManager.persist(location);
			entityManager.getTransaction().commit();
			entityManager.close();
		} catch (Exception ex) {
			entityManager.getTransaction().rollback();
		}
	}

	public Location searchLocationByName(String name) {
		final EntityManager entityManager = this.entityManagerFactory.createEntityManager();

		Location location = null;
		try {
			location = (Location) entityManager
					.createQuery("FROM Location u WHERE lower(u.name) = '" + name.toLowerCase() + "'")
					.getSingleResult();

			entityManager.close();
		} catch (Exception ex) {
			location = null;
		}

		return location;
	}

	public int getSize() {
		final EntityManager entityManager = this.entityManagerFactory.createEntityManager();
		int size = 0;
		try {
			size = entityManager.createQuery("FROM Location").getResultList().size();

			entityManager.close();
		} catch (Exception ex) {
			size = 0;
		}
		return size;
	}
}
