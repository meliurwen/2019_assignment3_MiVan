package mivan.repository;

import mivan.model.Loan;

public interface LoanRepository extends Repository<Loan, Long> {

	public void deleteLoanById(Long id);

	public void addLoan(Loan loan);

	public void updateLoan(Long id, String stato);

	public int getSize();

}
