package mivan.repository;

import java.util.List;
import java.util.Optional;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

import mivan.model.Author;
import mivan.model.Book;

public class BookRepositoryImpl implements BookRepository {

	private EntityManagerFactory entityManagerFactory;

	public BookRepositoryImpl() {
		this.entityManagerFactory = Persistence.createEntityManagerFactory("mivan");
	}

	@Override
	public Optional<Book> findById(Long id) {
		final EntityManager entityManager = this.entityManagerFactory.createEntityManager();
		Book book = entityManager.find(Book.class, id);
		entityManager.close();
		return Optional.ofNullable(book);
	}

	@Override
	public Iterable<Book> findAll() {
		final EntityManager entityManager = this.entityManagerFactory.createEntityManager();
		List<Book> book = entityManager.createQuery("FROM Book", Book.class).getResultList();
		entityManager.close();
		return book;
	}

	@Override
	public void deleteBookById(Long isbn) {
		final EntityManager entityManager = this.entityManagerFactory.createEntityManager();
		try {
			if (!entityManager.getTransaction().isActive()) {
				entityManager.getTransaction().begin();
			}
			Book book = entityManager.find(Book.class, isbn);
			entityManager.remove(book);
			entityManager.getTransaction().commit();
			entityManager.close();
		} catch (Exception ex) {
			entityManager.getTransaction().rollback();
		}
	}

	@Override
	public void addBook(Book book) {
		final EntityManager entityManager = this.entityManagerFactory.createEntityManager();
		try {
			if (!entityManager.getTransaction().isActive()) {
				entityManager.getTransaction().begin();
			}
			entityManager.persist(book);
			entityManager.getTransaction().commit();
			entityManager.close();

		} catch (Exception ex) {
			entityManager.getTransaction().rollback();
		}
	}

	@Override
	public void updateBook(Long isbn, String title, Book prequel) {
		final EntityManager entityManager = this.entityManagerFactory.createEntityManager();
		try {
			if (!entityManager.getTransaction().isActive()) {
				entityManager.getTransaction().begin();
			}
			Book book = entityManager.find(Book.class, isbn);
			book.setTitle(title);
			entityManager.persist(book);
			if (prequel != null) {
				book.setPrequel(prequel);
			}
			entityManager.getTransaction().commit();
			entityManager.close();
		} catch (Exception ex) {
			entityManager.getTransaction().rollback();
		}
	}

	@Override
	public Book searchBookByTitle(String title) {
		final EntityManager entityManager = this.entityManagerFactory.createEntityManager();

		Book book = null;
		try {
			book = (Book) entityManager.createQuery("FROM Book u WHERE lower(u.title) = '" + title.toLowerCase() + "'")
					.getSingleResult();

			entityManager.close();
		} catch (Exception ex) {
			book = null;
		}

		return book;
	}

	@Override
	public int getSize() {
		final EntityManager entityManager = this.entityManagerFactory.createEntityManager();
		int size = 0;
		try {
			size = entityManager.createQuery("FROM Book").getResultList().size();

			entityManager.close();
		} catch (Exception ex) {
			size = 0;
		}
		return size;
	}

	@Override
	public void addAuthor(Long isbn, Long id) {
		final EntityManager entityManager = this.entityManagerFactory.createEntityManager();
		try {
			if (!entityManager.getTransaction().isActive()) {
				entityManager.getTransaction().begin();
			}
			Book book = entityManager.find(Book.class, isbn);
			Author author = entityManager.find(Author.class, id);
			book.addAuthor(author);
			entityManager.persist(book);
			entityManager.persist(author);
			entityManager.getTransaction().commit();
			entityManager.close();
		} catch (Exception ex) {
			entityManager.getTransaction().rollback();
		}
	}

	@Override
	public void removeAuthor(Long isbn, Long id) {
		final EntityManager entityManager = this.entityManagerFactory.createEntityManager();
		try {
			if (!entityManager.getTransaction().isActive()) {
				entityManager.getTransaction().begin();
			}
			Book book = entityManager.find(Book.class, isbn);
			Author author = entityManager.find(Author.class, id);
			book.removeAuthor(author);
			entityManager.persist(book);
			entityManager.persist(author);
			entityManager.getTransaction().commit();
			entityManager.close();
		} catch (Exception ex) {
			entityManager.getTransaction().rollback();
		}
	}

}
