package mivan.repository;

import mivan.model.Author;

public interface AuthorRepository extends Repository<Author, Long> {

	public void deleteAuthorById(Long id);

	public void addAuthor(Author author);

	public void updateAuthor(Long id, String name);

	public Author searchAuthorByName(String name);

	public void addBook(Long isbn, Long id);

	public void removeBook(Long isbn, Long id);

	public int getSize();
}
