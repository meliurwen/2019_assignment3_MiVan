package mivan.repository;

import java.util.List;
import java.util.Optional;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

import mivan.model.Loan;

public class LoanRepositoryImpl implements LoanRepository {

	private EntityManagerFactory entityManagerFactory;

	public LoanRepositoryImpl() {
		this.entityManagerFactory = Persistence.createEntityManagerFactory("mivan");
	}

	public Optional<Loan> findById(Long id) {
		final EntityManager entityManager = this.entityManagerFactory.createEntityManager();
		Loan loan = entityManager.find(Loan.class, id);
		entityManager.close();
		return Optional.ofNullable(loan);
	}

	@Override
	public Iterable<Loan> findAll() {
		final EntityManager entityManager = this.entityManagerFactory.createEntityManager();
		List<Loan> loan = entityManager.createQuery("FROM Loan", Loan.class).getResultList();
		entityManager.close();
		return loan;
	}

	@Override
	public void deleteLoanById(Long id) {
		final EntityManager entityManager = this.entityManagerFactory.createEntityManager();
		try {
			if (!entityManager.getTransaction().isActive()) {
				entityManager.getTransaction().begin();
			}
			Loan loan = entityManager.find(Loan.class, id);
			entityManager.remove(loan);
			entityManager.getTransaction().commit();
			entityManager.close();
		} catch (Exception ex) {
			entityManager.getTransaction().rollback();
		}
	}

	@Override
	public void addLoan(Loan loan) {
		final EntityManager entityManager = this.entityManagerFactory.createEntityManager();
		try {
			if (!entityManager.getTransaction().isActive()) {
				entityManager.getTransaction().begin();
			}
			entityManager.persist(loan);
			entityManager.getTransaction().commit();
			entityManager.close();

		} catch (Exception ex) {
			entityManager.getTransaction().rollback();
		}
	}

	@Override
	public void updateLoan(Long id, String state) {
		final EntityManager entityManager = this.entityManagerFactory.createEntityManager();
		try {
			if (!entityManager.getTransaction().isActive()) {
				entityManager.getTransaction().begin();
			}
			Loan loan = entityManager.find(Loan.class, id);
			loan.setState(state);
			entityManager.persist(loan);
			entityManager.getTransaction().commit();
			entityManager.close();
		} catch (Exception ex) {
			entityManager.getTransaction().rollback();
		}
	}

	@Override
	public int getSize() {
		final EntityManager entityManager = this.entityManagerFactory.createEntityManager();
		int size = 0;
		try {
			size = entityManager.createQuery("FROM Loan").getResultList().size();

			entityManager.close();
		} catch (Exception ex) {
			size = 0;
		}
		return size;
	}
}
