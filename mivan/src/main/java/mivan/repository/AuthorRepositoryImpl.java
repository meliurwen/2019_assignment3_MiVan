package mivan.repository;

import java.util.List;
import java.util.Optional;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

import mivan.model.Author;
import mivan.model.Book;

public class AuthorRepositoryImpl implements AuthorRepository {

	private EntityManagerFactory entityManagerFactory;

	public AuthorRepositoryImpl() {
		this.entityManagerFactory = Persistence.createEntityManagerFactory("mivan");
	}

	@Override
	public Optional<Author> findById(Long id) {
		final EntityManager entityManager = this.entityManagerFactory.createEntityManager();
		Author author = entityManager.find(Author.class, id);
		entityManager.close();
		return Optional.ofNullable(author);
	}

	@Override
	public Iterable<Author> findAll() {
		final EntityManager entityManager = this.entityManagerFactory.createEntityManager();
		List<Author> author = entityManager.createQuery("FROM Author", Author.class).getResultList();
		entityManager.close();
		return author;
	}

	@Override
	public void deleteAuthorById(Long id) {
		final EntityManager entityManager = this.entityManagerFactory.createEntityManager();
		try {
			if (!entityManager.getTransaction().isActive()) {
				entityManager.getTransaction().begin();
			}
			Author author = entityManager.find(Author.class, id);
			entityManager.remove(author);
			entityManager.getTransaction().commit();
			entityManager.close();
		} catch (Exception ex) {
			entityManager.getTransaction().rollback();
		}
	}

	@Override
	public void addAuthor(Author author) {
		final EntityManager entityManager = this.entityManagerFactory.createEntityManager();
		try {
			if (!entityManager.getTransaction().isActive()) {
				entityManager.getTransaction().begin();
			}
			entityManager.persist(author);
			entityManager.getTransaction().commit();
			entityManager.close();

		} catch (Exception ex) {
			entityManager.getTransaction().rollback();
		}
	}

	@Override
	public void updateAuthor(Long id, String name) {
		final EntityManager entityManager = this.entityManagerFactory.createEntityManager();
		try {
			if (!entityManager.getTransaction().isActive()) {
				entityManager.getTransaction().begin();
			}
			Author author = entityManager.find(Author.class, id);
			author.setName(name);
			entityManager.persist(author);
			entityManager.getTransaction().commit();
			entityManager.close();
		} catch (Exception ex) {
			entityManager.getTransaction().rollback();
		}
	}

	@Override
	public Author searchAuthorByName(String name) {
		final EntityManager entityManager = this.entityManagerFactory.createEntityManager();

		Author author = null;
		try {
			author = (Author) entityManager
					.createQuery("FROM Author u WHERE lower(u.name) = '" + name.toLowerCase() + "'").getSingleResult();

			entityManager.close();
		} catch (Exception ex) {
			author = null;
		}

		return author;
	}

	@Override
	public int getSize() {
		final EntityManager entityManager = this.entityManagerFactory.createEntityManager();
		int size = 0;
		try {
			size = entityManager.createQuery("FROM Author").getResultList().size();

			entityManager.close();
		} catch (Exception ex) {
			size = 0;
		}
		return size;
	}

	@Override
	public void addBook(Long isbn, Long id) {
		final EntityManager entityManager = this.entityManagerFactory.createEntityManager();
		try {
			if (!entityManager.getTransaction().isActive()) {
				entityManager.getTransaction().begin();
			}
			Book book = entityManager.find(Book.class, isbn);
			Author author = entityManager.find(Author.class, id);
			book.addAuthor(author);
			entityManager.persist(book);
			entityManager.persist(author);
			entityManager.getTransaction().commit();
			entityManager.close();
		} catch (Exception ex) {
			entityManager.getTransaction().rollback();
		}
	}

	@Override
	public void removeBook(Long isbn, Long id) {
		final EntityManager entityManager = this.entityManagerFactory.createEntityManager();
		try {
			if (!entityManager.getTransaction().isActive()) {
				entityManager.getTransaction().begin();
			}
			Book book = entityManager.find(Book.class, isbn);
			Author author = entityManager.find(Author.class, id);
			book.removeAuthor(author);
			entityManager.persist(book);
			entityManager.persist(author);
			entityManager.getTransaction().commit();
			entityManager.close();
		} catch (Exception ex) {
			entityManager.getTransaction().rollback();
		}
	}

}
