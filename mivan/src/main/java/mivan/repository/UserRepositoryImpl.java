package mivan.repository;

import java.util.List;
import java.util.Optional;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

import mivan.model.User;

public class UserRepositoryImpl implements UserRepository {

	private EntityManagerFactory entityManagerFactory;

	public UserRepositoryImpl() {
		this.entityManagerFactory = Persistence.createEntityManagerFactory("mivan");
	}

	@Override
	public Optional<User> findById(Long id) {
		final EntityManager entityManager = this.entityManagerFactory.createEntityManager();
		User user = entityManager.find(User.class, id);
		entityManager.close();
		return Optional.ofNullable(user);
	}

	@Override
	public Iterable<User> findAll() {
		final EntityManager entityManager = this.entityManagerFactory.createEntityManager();
		List<User> users = entityManager.createQuery("FROM User", User.class).getResultList();
		entityManager.close();
		return users;
	}

	public void deleteUserById(Long id) {
		final EntityManager entityManager = this.entityManagerFactory.createEntityManager();
		try {
			if (!entityManager.getTransaction().isActive()) {
				entityManager.getTransaction().begin();
			}
			User user = entityManager.find(User.class, id);
			entityManager.remove(user);
			entityManager.getTransaction().commit();
			entityManager.close();
		} catch (Exception ex) {
			entityManager.getTransaction().rollback();
		}
	}

	public void addUser(User user) {
		final EntityManager entityManager = this.entityManagerFactory.createEntityManager();
		try {
			if (!entityManager.getTransaction().isActive()) {
				entityManager.getTransaction().begin();
			}
			entityManager.persist(user);
			entityManager.getTransaction().commit();
			entityManager.close();

		} catch (Exception ex) {
			entityManager.getTransaction().rollback();
		}
	}

	public void updateUser(Long id, String firstname, String lastname, String city, String username, String email,
			String password) {
		final EntityManager entityManager = this.entityManagerFactory.createEntityManager();
		try {
			if (!entityManager.getTransaction().isActive()) {
				entityManager.getTransaction().begin();
			}
			User user = entityManager.find(User.class, id);
			user.setFirstName(firstname);
			user.setLastName(lastname);
			user.setCity(city);
			user.setUsername(username);
			user.setEmail(email);
			user.setPassword(password);
			entityManager.persist(user);
			entityManager.getTransaction().commit();
			entityManager.close();
		} catch (Exception ex) {
			entityManager.getTransaction().rollback();
		}
	}

	public User searchUserByName(String username) {
		final EntityManager entityManager = this.entityManagerFactory.createEntityManager();

		User user = null;
		try {
			user = (User) entityManager
					.createQuery("FROM User u WHERE lower(u.username) = '" + username.toLowerCase() + "'")
					.getSingleResult();

			entityManager.close();
		} catch (Exception ex) {
			user = null;
		}

		return user;
	}

	public User searchUserByEmail(String email) {
		final EntityManager entityManager = this.entityManagerFactory.createEntityManager();

		User user = null;
		try {
			user = (User) entityManager.createQuery("FROM User u WHERE lower(u.email) = '" + email.toLowerCase() + "'")
					.getSingleResult();

			entityManager.close();
		} catch (Exception ex) {
			user = null;
		}

		return user;
	}

	public int getSize() {
		final EntityManager entityManager = this.entityManagerFactory.createEntityManager();
		int size = 0;
		try {
			size = entityManager.createQuery("FROM User").getResultList().size();

			entityManager.close();
		} catch (Exception ex) {
			size = 0;
		}
		return size;
	}
}
