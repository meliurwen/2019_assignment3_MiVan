package mivan.repository;

import java.util.List;

import mivan.model.Location;
import mivan.model.Staff;

public interface LocationRepository extends Repository<Location, Long> {

	public void deleteLocationById(Long id);

	public void addLocation(Location user);

	public void updateLocation(Long id, String name, String adress, List<Staff> staffs);

	public Location searchLocationByName(String name);

	public int getSize();

}
