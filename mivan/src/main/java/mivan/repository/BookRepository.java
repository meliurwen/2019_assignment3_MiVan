package mivan.repository;

import mivan.model.Book;

public interface BookRepository extends Repository<Book, Long> {

	// public Optional<AddressUser> findUserAddressById(Long id);

	public void deleteBookById(Long isbn);

	public void addBook(Book book);

	public void updateBook(Long isbn, String title, Book prequel);

	public Book searchBookByTitle(String title);

	public void addAuthor(Long isbn, Long id);

	public void removeAuthor(Long isbn, Long id);

	public int getSize();
}
