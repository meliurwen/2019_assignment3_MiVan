package mivan.repository;

import mivan.model.User;

public interface UserRepository extends Repository<User, Long> {

	// public Optional<AddressUser> findUserAddressById(Long id);

	public void deleteUserById(Long id);

	public void addUser(User user);

	public void updateUser(Long id, String firstname, String lastname, String city, String username, String email,
			String password);

	public User searchUserByName(String username);

	public User searchUserByEmail(String email);

	public int getSize();
}
