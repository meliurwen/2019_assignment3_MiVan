package mivan;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

import java.util.Optional;

import org.junit.jupiter.api.Order;
import org.junit.jupiter.api.Test;

import mivan.model.Item;
import mivan.model.Loan;
import mivan.model.Staff;
import mivan.model.User;
import mivan.repository.LoanRepository;
import mivan.repository.LoanRepositoryImpl;

public class LoanTest {

	private LoanRepository loanRepository = new LoanRepositoryImpl();

	@Test
	@Order(1)
	void testAddLoan() {

		long date_start = 1234;
		long date_end = 2345;
		String state = "loaning";

		Item item = null;
		Staff staff = null;
		User user = null;

		Loan loan = new Loan(date_start, date_end, state, item, staff, user);

		loanRepository.addLoan(loan);

		int size = loanRepository.getSize();

		assertEquals(size, 1);

	}

	@Test
	@Order(2)
	void testUpdateLoan() {

		long date_start = 1234;
		long date_end = 2345;
		String state = "loaning";

		Item item = null;
		Staff staff = null;
		User user = null;

		Loan loan = new Loan(date_start, date_end, state, item, staff, user);

		loanRepository.addLoan(loan);

		String update_state = "returned";

		loanRepository.updateLoan(0L, update_state);

		Optional<Loan> opt_loan = loanRepository.findById(0L);

		Loan loan2 = opt_loan.get();
		assertTrue(update_state.equals(loan2.getState()));

	}

	@Test
	@Order(3)
	void testDeleteLoan() {

		int start_size = loanRepository.getSize();
		long date_start = 1876;
		long date_end = 9876;
		String state = "loaning";

		Item item = null;
		Staff staff = null;
		User user = null;

		Loan loan = new Loan(date_start, date_end, state, item, staff, user);

		loanRepository.addLoan(loan);

		Optional<Loan> opt_loan2 = loanRepository.findById(0L);

		Loan book2 = opt_loan2.get();
		long ID = book2.getId();

		loanRepository.deleteLoanById(ID);

		int size = loanRepository.getSize();

		assertEquals(start_size, size);
	}
}
