package mivan;

import static org.junit.jupiter.api.Assertions.assertEquals;

import java.util.Optional;

import org.junit.jupiter.api.Order;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestMethodOrder;
import org.junit.jupiter.api.MethodOrderer.OrderAnnotation;

import mivan.model.Book;
import mivan.model.Item;
import mivan.model.Location;
import mivan.repository.ItemRepository;
import mivan.repository.ItemRepositoryImpl;

@TestMethodOrder(OrderAnnotation.class)
public class ItemTest {
	private ItemRepository itemRepository = new ItemRepositoryImpl();

	@Test
	@Order(1)
	void testAddItem() {
		Book book = null;
		Location location = null;
		Item item = new Item(book, location);
		itemRepository.addItem(item);
		int size = itemRepository.getSize();
		assertEquals(1, size);
	}

	@Test
	@Order(2)
	void testRemoveItem() {
		int startSize = itemRepository.getSize();
		Book book = null;
		Location location = null;
		Item item = new Item(book, location);
		itemRepository.addItem(item);
		int size = itemRepository.getSize();
		assertEquals(startSize + 1, size);

		Optional<Item> optItem2 = itemRepository.findById(item.getId());

		Item item2 = optItem2.get();
		long ID = item2.getId();
		itemRepository.deleteItemById(ID);

		int finalSize = itemRepository.getSize();
		assertEquals(startSize, finalSize);

	}
}