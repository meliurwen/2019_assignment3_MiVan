package mivan;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

import java.util.Optional;

import org.junit.jupiter.api.Order;
import org.junit.jupiter.api.Test;

import mivan.model.Book;
import mivan.repository.BookRepository;
import mivan.repository.BookRepositoryImpl;

public class BookTest {

	private BookRepository bookRepository = new BookRepositoryImpl();

	@Test
	@Order(1)
	void testAddBook() {

		long isbn = 1234;
		String title = "illiade";
		Book prequel = null;

		Book book = new Book(isbn, title, prequel);

		bookRepository.addBook(book);

		int size = bookRepository.getSize();

		assertEquals(size, 1);

	}

	@Test
	@Order(2)
	void testUpdateBook() {
		long isbn = 12348;
		String title = "odissea";
		Book prequel = null;

		Book book = new Book(isbn, title, prequel);

		bookRepository.addBook(book);
		String update_title = "eneide";
		bookRepository.updateBook(isbn, update_title, prequel);

		Optional<Book> opt_book2 = bookRepository.findById(isbn);

		Book book2 = opt_book2.get();
		assertTrue(update_title.equals(book2.getTitle()));

	}

	@Test
	@Order(3)
	void testDeleteBook() {
		int start_size = bookRepository.getSize();
		long isbn = 12345;
		String title = "illiade";
		Book prequel = null;

		Book book = new Book(isbn, title, prequel);

		bookRepository.addBook(book);

		Optional<Book> opt_book2 = bookRepository.findById(isbn);

		Book book2 = opt_book2.get();
		long ID = book2.getIsbn();

		bookRepository.deleteBookById(ID);

		int size = bookRepository.getSize();
		assertEquals(start_size, size);
	}

}
