package mivan;

import static org.junit.jupiter.api.Assertions.assertEquals;

import java.util.Optional;

import org.junit.jupiter.api.Order;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestMethodOrder;
import org.junit.jupiter.api.MethodOrderer.OrderAnnotation;

import mivan.model.Author;
import mivan.repository.AuthorRepository;
import mivan.repository.AuthorRepositoryImpl;

@TestMethodOrder(OrderAnnotation.class)
public class AuthorTest {

	private AuthorRepository authorRepository = new AuthorRepositoryImpl();

	@Test
	@Order(1)
	void testAddAuthor() {
		String name = "Ivan";
		Author author = new Author(name);
		authorRepository.addAuthor(author);
		int size = authorRepository.getSize();
		assertEquals(size, 1);
	}

	@Test
	@Order(2)
	void testUpdateAuthor() {
		String name = "Ivan";
		Author author = new Author(name);
		authorRepository.addAuthor(author);
		String updatedName = "Javoso";
		authorRepository.updateAuthor(author.getId(), updatedName);
		Optional<Author> updated_author = authorRepository.findById(author.getId());
		updated_author.ifPresent(a -> {
			assertEquals(a.getName(), updatedName);
		});

	}

	@Test
	@Order(3)
	void testDeleteAuthor() {
		int size = authorRepository.getSize();
		String name = "gollum";
		Author author = new Author(name);
		authorRepository.addAuthor(author);
		Author editAuthor = authorRepository.searchAuthorByName(name);
		long ID = editAuthor.getId();
		authorRepository.deleteAuthorById(ID);
		int end_size = authorRepository.getSize();
		assertEquals(end_size, size);
	}

}
