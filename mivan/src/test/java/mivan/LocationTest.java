package mivan;

import static org.junit.jupiter.api.Assertions.assertEquals;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.junit.jupiter.api.Order;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestMethodOrder;
import org.junit.jupiter.api.MethodOrderer.OrderAnnotation;

import mivan.model.Location;
import mivan.model.Staff;
import mivan.repository.LocationRepository;
import mivan.repository.LocationRepositoryImpl;
import mivan.repository.StaffRepository;
import mivan.repository.StaffRepositoryImpl;

@TestMethodOrder(OrderAnnotation.class)
public class LocationTest {

	private StaffRepository staffRepository = new StaffRepositoryImpl();
	private LocationRepository locationRepository = new LocationRepositoryImpl();

	@Test
	@Order(1)
	void testAddLocation() {

		String lname = "test_add_location_name";
		String ladress = "test_add_location_adress";
		Location location = new Location(lname, ladress);
		int size = locationRepository.getSize();
		locationRepository.addLocation(location);
		int newsize = locationRepository.getSize();
		assertEquals(newsize, size + 1);
	}

	@Test
	@Order(2)
	void testUpdateLocation() {
		// creo nuovo menìmbro dello staff
		String firstname1 = "test_update_location_name1";
		String lastname1 = "test_update_location_lastname1";
		String city1 = "test_update_location_city1";
		String idka1 = "test_update_location_idka1";
		String ruolo1 = "test_update_location_role1";
		Staff newstaff1 = new Staff(firstname1, lastname1, city1, idka1, ruolo1);

		String firstname2 = "test_update_location_name2";
		String lastname2 = "test_update_location_lastname2";
		String city2 = "test_update_location_city2";
		String idka2 = "test_update_location_idka2";
		String ruolo2 = "test_update_location_role2";
		Staff newstaff2 = new Staff(firstname2, lastname2, city2, idka2, ruolo2);

		staffRepository.addStaff(newstaff1);
		staffRepository.addStaff(newstaff2);

		List<Staff> staffs = new ArrayList<Staff>();
		staffs.add(newstaff1);
		staffs.add(newstaff2);

		// creo una locazione
		String lname = "test_update_location_name";
		String ladress = "test_update_location_adress";
		Location newlocation = new Location(lname, ladress);
		locationRepository.addLocation(newlocation);

		Location editLocation = locationRepository.searchLocationByName(lname);

		long id = editLocation.getId();
		// nuovi dati per modificare i vecchi
		String newlname = "test_update_location_newname";
		String newladress = "test_update_location_newadress";

		// aggungo i membri dello staff / modifico i dati della location
		locationRepository.updateLocation(id, newlname, newladress, staffs);

		Optional<Location> location = locationRepository.findById(id);
		location.ifPresent(a -> {
			assertEquals(a.getName(), newlname);
			assertEquals(a.getAdress(), newladress);
		});
	}

	@Test
	@Order(3)
	void testDeleteLocation() {
		String lname = "test_delete_location_name";
		String ladress = "test_delete_location_adress";
		Location location = new Location(lname, ladress);
		locationRepository.addLocation(location);
		Location removeLocation = locationRepository.searchLocationByName(lname);
		long ID = removeLocation.getId();
		int size = locationRepository.getSize();
		locationRepository.deleteLocationById(ID);
		int newsize = locationRepository.getSize();
		assertEquals(newsize, size - 1);
	}

}
