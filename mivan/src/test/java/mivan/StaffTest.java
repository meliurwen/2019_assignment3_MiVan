package mivan;

import static org.junit.jupiter.api.Assertions.assertEquals;

import java.util.Optional;

import org.junit.jupiter.api.Order;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestMethodOrder;
import org.junit.jupiter.api.MethodOrderer.OrderAnnotation;

import mivan.model.Location;
import mivan.model.Staff;
import mivan.repository.LocationRepository;
import mivan.repository.LocationRepositoryImpl;
import mivan.repository.StaffRepository;
import mivan.repository.StaffRepositoryImpl;

@TestMethodOrder(OrderAnnotation.class)
public class StaffTest {

	private StaffRepository staffRepository = new StaffRepositoryImpl();
	private LocationRepository locationRepository = new LocationRepositoryImpl();

	@Test
	@Order(1)
	void testAddStaff() {

		String firstname = "test_add_staff_name";
		String lastname = "test_add_staff_lastname";
		String city = "test_add_staff_city";
		String idka = "test_add_staff_idka";
		String ruolo = "test_add_staff_role";

		String lname = "test_add_staff_loc_name";
		String ladress = "test_add_staff_loc_adress";
		Location location = new Location(lname, ladress);
		locationRepository.addLocation(location);

		Staff newstaff = new Staff(firstname, lastname, city, idka, ruolo, location);

		int size = staffRepository.getSize();

		staffRepository.addStaff(newstaff);

		int newsize = staffRepository.getSize();

		assertEquals(newsize, size + 1);
	}

	@Test
	@Order(2)
	void testUpdateStaff() {

		// creo una locazione
		String lname = "test_update_staff_loc_name";
		String ladress = "test_update_staff_loc_adress";
		Location location = new Location(lname, ladress);

		// creo una seconda locazione
		String newlName = "test_update_staff_loc_newname";
		String newlAdress = "test_update_staff_loc_newname";
		Location newlocation = new Location(newlName, newlAdress);

		int losize = locationRepository.getSize();

		locationRepository.addLocation(location);
		locationRepository.addLocation(newlocation);

		int newlosize = locationRepository.getSize();

		assertEquals(newlosize, losize + 2); // ci sono 2 locazioni aggunte

		// creo un membro dello staff
		String firstname = "test_update_staff_name";
		String lastname = "test_update_staff_lastname";
		String city = "test_update_staff_city";
		String idka = "test_update_staff_idka";
		String ruolo = "test_update_staff_role";

		Staff newstaff = new Staff(firstname, lastname, city, idka, ruolo);

		int size = staffRepository.getSize();

		staffRepository.addStaff(newstaff);

		int newsize = staffRepository.getSize();

		assertEquals(newsize, size + 1); // c'è 1 nuovo membro dello staff

		Staff editStaff = staffRepository.searchStaffByidka(idka);

		long id = editStaff.getId();
		String newfirstname = "test_update_staff_newname";
		String newlastname = "test_update_staff_newlastname";
		String newcity = "test_update_staff_newcity";
		String newidka = "test_update_staff_newidka";
		String newruolo = "test_update_staff_newrole";

		staffRepository.updateStaff(id, newfirstname, newlastname, newcity, newidka, newruolo, newlocation);

		Optional<Staff> staff = staffRepository.findById(id);
		staff.ifPresent(a -> {
			assertEquals(a.getFirstName(), newfirstname);
			assertEquals(a.getLastName(), newlastname);
			assertEquals(a.getCity(), newcity);
			assertEquals(a.getIdka(), newidka);
			assertEquals(a.getRuolo(), newruolo);
			assertEquals(a.getLocation().getName(), newlName);
			assertEquals(a.getLocation().getAdress(), newlAdress);

		});
	}

	@Test
	@Order(3)
	void testDeleteStaff() {

		String firstname = "test_delete_staff_name";
		String lastname = "test_delete_staff_lastname";
		String city = "test_delete_staff_city";
		String idka = "test_delete_staff_idka";
		String ruolo = "test_delete_staff_role";

		String lname = "test_delete_staff_loc_name";
		String ladress = "test_delete_staff_loc_adress";
		Location location = new Location(lname, ladress);
		locationRepository.addLocation(location);

		Staff newstaff = new Staff(firstname, lastname, city, idka, ruolo, location);

		staffRepository.addStaff(newstaff);

		Staff editStaff = staffRepository.searchStaffByidka(idka);

		long ID = editStaff.getId();

		staffRepository.deleteStaffById(ID);
		;

		int size = staffRepository.getSize();
		assertEquals(size, 0);
	}

}
