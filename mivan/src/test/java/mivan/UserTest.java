package mivan;

import static org.junit.jupiter.api.Assertions.*;

import java.util.Optional;

import org.junit.jupiter.api.MethodOrderer.OrderAnnotation;

import mivan.model.User;
import mivan.repository.UserRepository;
import mivan.repository.UserRepositoryImpl;

import org.junit.jupiter.api.Order;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestMethodOrder;

@TestMethodOrder(OrderAnnotation.class)
class UserTest {

	private UserRepository userRepository = new UserRepositoryImpl();

	@Test
	@Order(1)
	void testAddUser() {

		String firstname = "test_add_user_name";
		String lastname = "test_add_user_lastname";
		String city = "test_add_user_city";
		String username = "test_add_user_username";
		String email = "test_add_user_email";
		String password = "test_add_user_password";
		User user = new User(firstname, lastname, city, username, email, password);

		int size = userRepository.getSize();

		userRepository.addUser(user);

		int newsize = userRepository.getSize();

		assertEquals(newsize, size + 1);
	}

	@Test
	@Order(2)
	void testUpdateUser() {

		String firstname = "test_update_user_name";
		String lastname = "test_update_user_lastname";
		String city = "test_update_user_city";
		String username = "test_update_user_username";
		String email = "test_update_user_email";
		String password = "test_update_user_password";

		User newuser = new User(firstname, lastname, city, username, email, password);

		userRepository.addUser(newuser);

		User editUser = userRepository.searchUserByName(username);

		long id = editUser.getId();

		String newfirstname = "test_update_user_newname";
		String newlastname = "test_update_user_newlastname";
		String newcity = "test_update_user_newcity";
		String newusername = "test_update_user_newusername";
		String newemail = "test_update_user_newemail";
		String newpassword = "test_update_user_newpassword";

		userRepository.updateUser(id, newfirstname, newlastname, newcity, newusername, newemail, newpassword);

		Optional<User> user = userRepository.findById(id);
		user.ifPresent(a -> {
			assertEquals(a.getFirstName(), newfirstname);
			assertEquals(a.getLastName(), newlastname);
			assertEquals(a.getCity(), newcity);
			assertEquals(a.getUsername(), newusername);
			assertEquals(a.getEmail(), newemail);
			assertEquals(a.getPassword(), newpassword);

		});
	}

	@Test
	@Order(3)
	void testDeleteUser() {

		String firstname = "test_delete_user_name";
		String lastname = "test_delete_user_lastname";
		String city = "test_delete_user_city";
		String username = "test_delete_user_username";
		String email = "test_delete_user_email";
		String password = "test_delete_user_password";

		User newuser = new User(firstname, lastname, city, username, email, password);

		userRepository.addUser(newuser);

		User editUser = userRepository.searchUserByName(username);

		long ID = editUser.getId();

		int size = userRepository.getSize();

		userRepository.deleteUserById(ID);

		int newsize = userRepository.getSize();
		assertEquals(newsize, size - 1);
	}
}
