<div align="center">
<img src="doc/img/logo.png" alt="Cognitio Logo" width="200" >
</div>

# 2019 Assignment 3

> **_Note:_** È una repo privata, usa l'utente `sw_dev_process_course` per accederci!

+ **_Link del Progetto:_** https://gitlab.com/meliurwen/2019_assignment3_MiVan
+ **_Link del Documento in PDF:_** https://gitlab.com/meliurwen/2019_assignment3_MiVan/blob/master/assignment3_mivan.pdf

## Applicazione

L'applicazione oggetto di questo assigment è **_MiVan_**.

Si tratta di un'applicazione che gestisce _prestiti di libri_ di un sistema bibliotecario _con una o più sedi_. Essa, oltre ad essere in grado di gestire i prestiti, è anche in grado di _gestire i libri_, la loro _posizione_, lo _staff_ che amministra i prestiti e gli utenti che ne fanno richiesta.

Grazie a questa applicazione è possibile _creare_, _visualizzzare_, _modificare_ e _rimuovere_ in tempo reale lo stato di prestito dei libri. I prestiti sono descritti da una _data di inizio_, _di fine_, uno _stato_, un _libro_, un _utente_ ed un _operatore_. Si ritiene che sia importante sottolineare che durante la progettazione si è presa la decisione che un prestito possa consistere in _esattamente una unità di libro_, in maniera tale che questo livello di granularità permetta all'utente, in caso di prestito contemporaneo di più libri, di _non_ doverli restituire tutti in blocco.

Altra caratteristica è la separazione tra concetto di libro ed "unità di libro", questo per gestire in maniera efficiente il caso molto frequente in cui il sistema bibliotecario possieda più copie dello stesso libro; nella nostra implementazione ogni singola unità (Item) corrisponderebbe in maniera univoca all'unità fisica corrispondente.

Una feature degna di nota (che andrebbe a soddisfare il requisito del self-loop) è la possibilità di sapere se nel sistema bibliotecario è disponibile il sequel (ammesso che esista) di un determinato libro.

Allo stato attuale l'applicazione è stata pensata per essere utilizzata nell'area metropolitana di _Brescia_ e _Novara_, con un _target iniziale_ ristretto agli utenti delle _sedi del sistema blibliotecario comunale_ delle rispettive città.

A seconda della trazione che potrebbe ricevere una volta lanciata, si potrà valutare un'eventuale _espansione_ del territorio coperto e degli enti (sia pubblici che privati) interessati.

## Membri del Gruppo

+ Salanti Michele - 793091
+ Donati Ivan - 781022

