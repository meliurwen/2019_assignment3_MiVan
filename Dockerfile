FROM openjdk:8-jdk-slim AS builder

COPY multibook /usr/src/

WORKDIR /usr/src/multibook

RUN apt-get update && apt-get install -y mvn && \
    ./mvnw clean package spring-boot:repackage

FROM openjdk:8-jdk-slim AS app

COPY --from=builder /usr/src/multibook/target/multibook.jar /usr/src/app.jar

WORKDIR /usr/src

ENTRYPOINT ["java", "-Djava.security.egd=file:/dev/./urandom", "-jar", "app.jar"]

